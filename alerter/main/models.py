from django.db import models
from django.contrib import admin

# Create your models here.

class Term(models.Model):
	term_id = models.IntegerField(primary_key = True)
	term_type = models.CharField(max_length = 10)
	title = models.CharField(max_length = 250)

class TaggedItem(models.Model):
	term = models.ForeignKey(Term)
	date = models.DateTimeField('Date retrieved')
	item_type = models.CharField(max_length = 20)
	title = models.CharField(max_length = 200)
	content = models.TextField()

class Subscription(models.Model):
	email_address = models.EmailField()
	term = models.ForeignKey(Term)

admin.site.register(Term)
admin.site.register(TaggedItem)
admin.site.register(Subscription)