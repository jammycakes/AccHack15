# ====== Methods to access DDP ====== #

from django.conf import settings

import requests
import json


"""
Queries DDP and returns the data from the requested dataset.
"""

def query_ddp(endpoint, **kwargs):
	url = settings.DDP_BASE_URL
	url += ('/' if settings.DDP_BASE_URL.endswith('/') else '')
	url += endpoint + '.json'
	headers = {
		'User-Agent': 'Topic Alerter',
		'Accept': 'application/json'
	}
	r = requests.get(url, params=kwargs)
	r.raise_for_status()
	return r.json()

def query_ddp_all(endpoint, page_size=100, **kwargs):
	page = 0
	count = 0
	kwargs['_pageSize'] = page_size
	kwargs['_page'] = page

	results = query_ddp(endpoint, **kwargs)
	while count < results['result']['totalResults']:
		for a in results['result']['items']:
			count += 1
			if type(a) is dict:
				yield a
		page += 1
		kwargs['_page'] = page
		results = query_ddp(endpoint, **kwargs)
