require.config({
  shim: {
    bootstrap: {
      deps: [
        'jquery'
      ]
    }
  },
  paths: {
    jquery: '../../lib/jquery/dist/jquery',
    requirejs: '../../lib/requirejs/require',
    bootstrap: '../../lib/bootstrap/dist/js/bootstrap',
    'es5-shim': '../../lib/es5-shim/es5-shim',
  },
  packages: [
  ]
});

(function(window, document) {

define(['jquery', 'bootstrap', 'ddp-queries'], function($, bootstrap, ddpQueries) {

  $('#q').change(function() {
    var searchText = $(this).val();
    console.log('Searching for ' + searchText);
    
    ddpQueries.findTerms(searchText)
      .done(function(x) {
        console.log(x);
      });
  });

});

})();