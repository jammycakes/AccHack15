define(['jquery', '../../app/loader'], function($, loader) {
	"use strict";
	
	function DdpQueries() {
		var self = this;

		self.findTerms = function(term, cls) {
			
			var args = {
				'_search': term
			};
			
			if (cls) args.class = cls;
			
			return loader.load('terms', args, function(x) {
				return x;
			});
		};
	}

	return new DdpQueries();
});