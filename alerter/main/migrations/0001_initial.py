# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('email_address', models.EmailField(max_length=254)),
            ],
        ),
        migrations.CreateModel(
            name='TaggedItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('date', models.DateTimeField(verbose_name='Date retrieved')),
                ('item_type', models.CharField(max_length=20)),
                ('title', models.CharField(max_length=200)),
                ('content', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Term',
            fields=[
                ('term_id', models.IntegerField(serialize=False, primary_key=True)),
                ('term_type', models.CharField(max_length=10)),
                ('title', models.CharField(max_length=250)),
            ],
        ),
        migrations.AddField(
            model_name='taggeditem',
            name='term',
            field=models.ForeignKey(to='main.Term'),
        ),
        migrations.AddField(
            model_name='subscription',
            name='term',
            field=models.ForeignKey(to='main.Term'),
        ),
    ]
